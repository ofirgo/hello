import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private path:string ='https://firebasestorage.googleapis.com/v0/b/hello-b2e08.appspot.com/o/';
  public images:string[] = [];
  constructor() { 
    this.images[0] = this.path + 'דינוזאור.png' + '?alt=media&token=fc814665-c99e-417d-ab11-20c2aeb0c9e8';
    this.images[1] = this.path +''+'';
    this.images[2] = this.path +''+'';
    this.images[3] = this.path +''+'';
    this.images[4] = this.path +''+'';
  }
}
