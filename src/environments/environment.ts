// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCyKnUMfOLUnKYT-TbZiu4LCDZTK8ZOENg",
    authDomain: "hello-b2e08.firebaseapp.com",
    databaseURL: "https://hello-b2e08.firebaseio.com",
    projectId: "hello-b2e08",
    storageBucket: "hello-b2e08.appspot.com",
    messagingSenderId: "1037574811205",
    appId: "1:1037574811205:web:b719310602552681203d2c"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
